
module.exports = {
  Query: {
    pets(_, {input}, ctx) {
      return ctx.models.Pet.findMany(input)
    },
    pet(_, {input}, ctx) {
      return ctx.models.Pet.findOne(input)
    }
  },
  Mutation: {
    newPet(_, {input}, ctx) {
      const result = ctx.models.Pet.create(input);
      return result
    }
  },
  // Pet: {
  //   // id(id) {
  //   //   models.Pet.findOne(id);
  //   // },
  //   img(pet) {
  //     return pet.type === 'DOG'
  //       ? 'https://placedog.net/300/300'
  //       : 'http://placekitten.com/300/300'
  //   }
  // },
  // User: {
  //   id() {
  //     models.User.findOne();
  //   }
  // }
}
