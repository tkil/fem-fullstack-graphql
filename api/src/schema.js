const { gql } = require('apollo-server')

/**
 * Type Definitions for our Schema using the SDL.
 */
const typeDefs = gql`
  type User {
    id: ID!
    username: String!
  }
  """
  My Pet
  """
  type Pet {
    id: ID!
    createdAt: String!
    name: String!
    type: String!
  }

  input PetInput {
    name: String
    type: String
  }

  input newPetInput {
    name: String!
    type: String!
  }

  type Query {
    pet(input: PetInput): Pet,
    pets(input: PetInput): [Pet]!
  }

  type Mutation {
    newPet(input: newPetInput!): Pet!
  }
`;

module.exports = typeDefs
