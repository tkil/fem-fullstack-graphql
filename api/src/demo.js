const { ApolloServer, gql } = require("apollo-server");

const typeDefs = gql`
  enum ShoeType {
    JORDAN
    NIKE
    ADIDDAS
  }

  type User {
    email: String!
    avatar: String
    friends: [User!]
  }

  type Shoe {
    brand: ShoeType!
    size: Int!
  }

  input ShoesInput {
    brand: ShoeType
    size: Int
  }

  input NewShoeInput {
    brand: ShoeType!
    size: Int!
  }

  type Query {
    me: User!
    shoes(input: ShoesInput): [Shoe]!
  }

  type Mutation {
    newShoe(input: ShoesInput!): Shoe!
  }
`;

const resolvers = {
  Query: {
    shoes(_, {input}) {
      const shoes = [
        { brand: "nike", size: 12 },
        { brand: "adiddas", size: 14 }
      ]
      return shoes.filter((s) => !input || (s.brand === input.brand) )
    },

    me() {
      return {
        avatar: 'http://yoda.png',
        friends: [],
      }
    }
  },
  
  Mutation: {
    newShoe(_, {input}) {
      return input;
    }
  }
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context() { }
})

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
})
